#Author: LACHI/AMIRAT/SAKHO/SAAL
@tag
Feature: En tant que Winx
				 Je veux une pièce adéquate à mon statut qui est défini par mon grade
				 Afin d’exercer mes pouvoirs correctement.
	@tag1
  Scenario Outline:
    Given Une winx est dans une pièce.
    When le directeur notifie que la winx a évolué d'une valeur <value>.
    Then Il faut que le status de la fenêtre soit <status> 
    
   Examples: 
      | value   | status  |
      |    6   | Ouverte  |
    