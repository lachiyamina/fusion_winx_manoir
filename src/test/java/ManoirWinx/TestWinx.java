package ManoirWinx;
/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestWinx {
	private Winx bloum;

	@Before
	public void setUp() // throws java.lang.Exception
	{
		this.bloum = new Winx("Fire", 7);
	}

	@After
	public void tearDown() // throws java.lang.Exception
	{
		// Libérez ici les ressources engagées par setUp()

	}

	@Test
	public void testSetPouvoir() {
		// when
		this.bloum.setPouvoir("Feux");

		// then
		assertEquals(this.bloum.getPouvoir(), "Feux");
	}

	@Test
	public void testSetGrade() {
		// when
		this.bloum.setGrade(3);

		// then
		assertEquals(this.bloum.getGrade(), 3);
	}

	@Test
	public void testGetGrade() {
		assertEquals(this.bloum.getGrade(), 7);
	}

	@Test
	public void testGetPouvoir() {
		assertEquals(this.bloum.getPouvoir(), "Fire");
	}

	@Test
	public void testGrade() {
		// this.bloum = new Winx("Fire", 7);
		this.bloum.setGrade(5);
		assertEquals("Moyenne", this.bloum.VerifierGrade());
	}

}
