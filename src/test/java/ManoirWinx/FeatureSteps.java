package ManoirWinx;
/*
 *   
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

import static org.junit.Assert.assertEquals;

import cucumber.api.PendingException;
import cucumber.api.java.en.*;

public class FeatureSteps {
	private Piece chambreFeu;

	@Given("Une winx est dans une pièce.")
	public void une_winx_est_dans_une_pièce() {
		chambreFeu = new ChambreFeu();
	}
	
	@When("le directeur notifie que la winx a évolué d'une valeur {int}.")
	public void le_directeur_notifie_que_la_winx_a_évolué_d_une_valeur(int int1) {
		chambreFeu.getWinx().setGrade(6);
		assertEquals(int1, chambreFeu.getWinx().getGrade());
	}

	@Then("Il faut que le status de la fenêtre soit Ouverte")
	public void il_faut_que_le_status_de_la_fenêtre_soit_Ouverte() {
		assertEquals("Ouverte", chambreFeu.getFenetre().etat());
	}

	@Then("Il faut que le status de la fenêtre soit Fermée")
	public void il_faut_que_le_status_de_la_fenêtre_soit_Fermée() {
		assertEquals("Fermée", chambreFeu.getFenetre().etat());
	}

	@When("Je demande d'afficher la description.")
	public void je_demande_d_afficher_la_description() {
		chambreFeu = new DecoPieceConcreteA(chambreFeu);
	}

	@Then("La description est affichée.")
	public void la_description_est_affichée() {
		assertEquals(
				"Cette pièce est un/une ChambreFeu elle contient 5 objets  et une Fenetre : Ouverte, et elle appartient à "
						+ " une fée avec le pouvoir de : Feu et de grade : 5",
				chambreFeu.description());

	}

}
