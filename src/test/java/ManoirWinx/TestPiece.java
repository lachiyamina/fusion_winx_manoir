package ManoirWinx;
/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPiece {

	private Piece chambreFeu;
	private Piece chambreNature;
	private Piece chambreGlace;
	private Winx flora;

	/**
	 * Constructeur de la classe-test TestPiece
	 */
	public TestPiece() {
	}

	/**
	 * Met en place les engagements.
	 *
	 * Méthode appelée avant chaque appel de méthode de test.
	 */
	@Before
	public void setUp() // throws java.lang.Exception
	{
		// Initialisez ici vos engagements
		flora = new Winx("Nature", 10);
		chambreFeu = new ChambreFeu();
		chambreNature = new ChambreNature();
		chambreGlace = new ChambreGlace();

	}

	/**
	 * Supprime les engagements
	 *
	 * Méthode appelée après chaque appel de méthode de test.
	 */
	@After
	public void tearDown() // throws java.lang.Exception
	{
		// Libérez ici les ressources engagées par setUp()
	}

	@Test
	public void testAjoutObjet() {
		assertEquals(10, chambreFeu.ajoutObjets(5));
		assertEquals(10, chambreGlace.ajoutObjets(5));
	}

	@Test
	public void testObjetsEtage() {
		Etage etage2 = new Etage();
		etage2.ajouterPiece(chambreFeu);
		etage2.ajouterPiece(chambreNature);
		etage2.ajouterPiece(chambreGlace);
		assertEquals(15, etage2.nbObjetsEtage());
	}
}
