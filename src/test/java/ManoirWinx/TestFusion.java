package ManoirWinx;
/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestFusion {

	private Piece chambreFeu;
	private Piece chambreNature;
	private Piece chambreGlace;
	private Winx flora;

	@Before
	public void setUp() // throws java.lang.Exception
	{
		// Initialisez ici vos engagements
		chambreFeu = new ChambreFeu();
		chambreNature = new ChambreNature();
		chambreGlace = new ChambreGlace();
	}

	@Test
	public void testDesriptionWithoutDecorator() {
		assertEquals("Cette pièce est un/une ChambreFeu elle contient 5 objets  et une Fenetre : Ouverte",
				chambreFeu.description());
	}

	@Test
	public void testDesriptionWithinDecorator() {
		chambreFeu = new DecoPieceConcreteA(chambreFeu);
		assertEquals(
				"Cette pièce est un/une ChambreFeu elle contient 5 objets  et une Fenetre : Ouverte, et elle appartient à  une fée avec le pouvoir de : Feu et de grade : 5",
				chambreFeu.description());
	}

	@Test
	public void testEtatFenetreFermee() {
		chambreFeu.getWinx().setGrade(3);
		chambreFeu.setWinx(chambreFeu.getWinx());
		assertEquals("Fermée", chambreFeu.getFenetre().etat());
	}

	@Test
	public void testEtatFenetreOuverte() {
		chambreFeu.getWinx().setGrade(10);
		chambreFeu.setWinx(chambreFeu.getWinx());
		assertEquals("Ouverte", chambreFeu.getFenetre().etat());
	}

}
