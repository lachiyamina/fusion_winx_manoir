package ManoirWinx;

/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */
public class DecoPieceConcreteA extends DecoratorPiece {

	private Piece p;

	public DecoPieceConcreteA(Piece p) {
		super();
		this.p = p;
	}

	public String description() {

		return p.description() + ", et elle appartient à  une fée avec le pouvoir de : " + p.getWinx().getPouvoir()
				+ " et de grade : " + p.getWinx().getGrade();
	}

}
