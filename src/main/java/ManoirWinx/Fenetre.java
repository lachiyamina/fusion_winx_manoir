package ManoirWinx;
/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

public class Fenetre {

	// private boolean fermé;
	private EtatFenetre etatFenetre; // interface Etatfenetre
	private int nbDeco;
	private boolean etat;

	public Fenetre() {
		this.nbDeco = 0;
	}

	public void setEtat(EtatFenetre newEtat) {

		this.etatFenetre = newEtat;
	}

	public boolean getEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public void action() {

		etatFenetre.action(this);
	}

	// Méthode qui permet de retourner l'état d'une fenêtre "Ouverte/Fermée"
	public String etat() {
		if (etat)
			return "Ouverte";
		return "Fermée";
	}

	public void ajoutDeco(int y) {
		this.nbDeco += y;
	}

}
