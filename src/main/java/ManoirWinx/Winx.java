package ManoirWinx;
/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

public class Winx {

	private String pouvoir;
	private int grade;
	private EcoleWinx ecole = new EcoleWinx();

	public Winx(String pouvoir, int grade) {
		// Initialisation des variables d'instance
		this.pouvoir = pouvoir;
		this.grade = grade;

	}

	// Setters & getters
	public int getGrade() {
		return this.grade;
	}

	public String getPouvoir() {
		return this.pouvoir;
	}

	public void setGrade(int grade) {
		this.grade = grade;
		ecole.affecterWinx(this);
	}

	public void setPouvoir(String pouvoir) {
		this.pouvoir = pouvoir;
	}

	// Afficher les caractéréstiques d'une Winx.
	public String Afficher() {
		return "Hello, je suis une fée de grade : " + this.grade + " et de pouvoir " + this.pouvoir;
	}

	// Méthode qui permet de vérifier le grade d'une Winx et de retourner son
	// status.
	public String VerifierGrade() {
		if (this.grade < 5) {
			return "Apprentie";
		} else if (this.grade == 5)
			return "Moyenne";
		return "Forte";
	}
}
