package ManoirWinx;

/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */
import java.util.ArrayList;

public class Etage {

	private ArrayList<Piece> listPiece;
	private int nb_Objects = 0;

	public Etage() {
		listPiece = new ArrayList<Piece>();
	}

	public void ajouterPiece(Piece p) {
		this.listPiece.add(p);
	}

	public int nbObjetsEtage() {
		for (int i = 0; i < this.listPiece.size(); i++) {
			nb_Objects += this.listPiece.get(i).getNbObjets();
		}
		return nb_Objects;

	}

}
