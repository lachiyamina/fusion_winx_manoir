package ManoirWinx;
/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

public abstract class Piece {
	private String nom;
	private int nbObjets;
	private Fenetre fenetre;
	private Winx winx;

	// Setters & Getters
	public String getNom() {
		return this.nom;
	}

	public int getNbObjets() {
		return this.nbObjets;
	}

	public void setNbObjets(int nb_objets) {
		this.nbObjets = nb_objets;
	}

	public void setNom(String new_nom) {
		this.nom = new_nom;
	}

	public Fenetre getFenetre() {
		return this.fenetre;
	}

	public void setFenetre() {
		this.fenetre = new Fenetre();
	}

	public Winx getWinx() {
		return this.winx;
	}

	public void setWinx(Winx winx) {
		this.winx = winx;
		int grade = this.getWinx().getGrade();
		setFenetre();
		if (grade >= 5) {
			this.getFenetre().setEtat(new OuvrirFenetre());
			this.getFenetre().action();
		} else {
			this.getFenetre().setEtat(new FermerFenetre());
			this.getFenetre().action();

		}
	}

	// Retourne la description d'une pièce.
	public String description() {
		return "Cette pièce est un/une " + getNom() + " elle contient " + getNbObjets()
				+ " objets  et une Fenetre : " + this.getFenetre().etat();
	}

	// Ajouter si la pièce a une fenetre et si elle est ouverte ou non.
	public int ajoutObjets(int y) {
		this.nbObjets += y;
		return this.nbObjets;
	}

	// Ajouter une décoration à une fenêtre.
	public int ajoutDecoFenetre(int y, Winx x) {
		this.fenetre.ajoutDeco(y);
		this.nbObjets += y;
		return this.nbObjets;
	}

}
