package ManoirWinx;

/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */
import java.util.ArrayList;

public class EcoleWinx {
	private ArrayList<Winx> sectionPrepa;
	private ArrayList<Winx> sectionMoyenne;
	private ArrayList<Winx> sectionForte;

	public EcoleWinx() {
		// Initialisation des variables d'instance
		sectionPrepa = new ArrayList<Winx>();
		sectionMoyenne = new ArrayList<Winx>();
		sectionForte = new ArrayList<Winx>();

	}

	// Méthode qui permet d'affecter une winx selon son grade.
	public void affecterWinx(Winx bloum) {
		if (bloum != null && bloum.getGrade() < 5) {
			sectionPrepa.add(bloum);
		} else if (bloum != null && bloum.getGrade() == 5) {
			sectionMoyenne.add(bloum);
		} else {
			sectionForte.add(bloum);
		}
	}

}
