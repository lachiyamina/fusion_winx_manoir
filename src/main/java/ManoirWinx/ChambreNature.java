package ManoirWinx;
/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */

public class ChambreNature extends Piece {
	// Initialisation de la pièce numéro 2
	public ChambreNature() {
		Winx flora = new Winx("Nature", 10);
		setNom("Cuisine");
		setNbObjets(5);
		setWinx(flora);
	} 

}
