package ManoirWinx;

/*
 * 
 * LACHI/SAAL/AMIRAT/SAKHO
 * 
 */
public class ChambreFeu extends Piece {
	// Initialisation de la pièce numéro 1
	public ChambreFeu() {
		Winx flora = new Winx("Feu", 5);
		setNom("ChambreFeu");
		setNbObjets(5);
		setWinx(flora);
	}

}
